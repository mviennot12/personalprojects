# Welcome to my Formula1 Analytics Dashboard

## Overview 

This project uses the FastF1 Python package to access telemetry, and various other data from Formula 1 race and
qualification sessions. 

The data is presented in an R Shiny (library for R) dashboard. The R library "Reticulate" is used to interface 
to Python directly in an R session. 

## Files

### global.R

### server.R

### ui.R

